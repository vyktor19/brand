<?php
class Frb_Brand_Model_Source_Option extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /*
     * This function is used to create the options for the brand attribute.
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $brands = Mage::getModel('brand/brand')->getCollection();
            $this->_options = array();
            foreach ($brands as $brand ){
                array_push($this->_options, array('value' => $brand->getBrandId(),
                                                  'label' => $brand->getName()));
            }
        }
        return $this->_options;
    }
}