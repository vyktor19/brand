<?php
class Frb_Brand_Block_Adminhtml_Brand_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if (Mage::getSingleton('adminhtml/session')->getExampleData())
        {
            $data = Mage::getSingleton('adminhtml/session')->getExamplelData();
            Mage::getSingleton('adminhtml/session')->getExampleData(null);
        }
        elseif (Mage::registry('brand_data'))
        {
            $data = Mage::registry('brand_data')->getData();
        }
        else
        {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        $fieldset = $form->addFieldset('brand_form', array(
            'legend' =>Mage::helper('brand')->__('Brand Information')
        ));

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('brand')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
            'note'     => Mage::helper('brand')->__('The name of the brand.'),
        ));

        $fieldset->addField('origin_country', 'select', array(
            'label'     => Mage::helper('brand')->__('Origin Country'),
            'class'     => 'required-entry',
            'values'    => array('RO' => 'RO', 'NA' => 'NA', 'GB' => 'GB'),
            'required'  => true,
            'name'      => 'origin_country',
        ));

        $fieldset->addField('level', 'select', array(
            'label'     => Mage::helper('brand')->__('Level'),
            'class'     => 'required-entry',
            'values'    => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10'),
            'value'     => '5',
            'required'  => true,
            'name'      => 'level',
        ));

        $fieldset->addField('website', 'text', array(
            'label'     => Mage::helper('brand')->__('Website'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'website',
        ));

        if (!empty($data)){
            $form->setValues($data);
        }

        return parent::_prepareForm();
    }
}