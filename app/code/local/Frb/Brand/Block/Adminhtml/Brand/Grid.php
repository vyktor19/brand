<?php

class Frb_Brand_Block_Adminhtml_Brand_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('brand_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('brand/brand')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('brand')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'brand_id',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('brand')->__('Name'),
            'align'     =>'left',
            'index'     => 'name',
        ));

        $this->addColumn('origin_country', array(
            'header'    => Mage::helper('brand')->__('Origin Country'),
            'align'     =>'left',
            'index'     => 'origin_country',
        ));

        $this->addColumn('level', array(
            'header'    => Mage::helper('brand')->__('Level'),
            'align'     => 'left',
            'index'     => 'level',
        ));

        $this->addColumn('website', array(
            'header'    => Mage::helper('brand')->__('Website'),
            'align'     => 'left',
            'index'     => 'website',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}