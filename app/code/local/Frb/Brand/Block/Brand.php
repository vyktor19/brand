<?php
class Frb_Brand_Block_Brand extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_brand';
        $this->_blockGroup = 'brand';
        $this->_headerText = Mage::helper('brand')->__('Brands');
    }

    public function getBrandCollection()
    {
        $config = Mage::helper('brand')->getBrandConfig();
        $collection = Mage::getModel('brand/brand')->getCollection()->addFieldToFilter('level',array("eq",$config));
        $collection->setOrder('name');
        return $collection;
    }
}
