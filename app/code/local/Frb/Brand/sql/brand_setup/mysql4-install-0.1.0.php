<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS `{$installer->getTable('brand/brand')}`;
    CREATE TABLE `{$installer->getTable('brand/brand')}` (
      `brand_id` int(11) NOT NULL auto_increment,
      `name` text,
      `origin_country` char(2),
      `level` int,
      `website` text,
      PRIMARY KEY  (`brand_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    INSERT INTO `{$installer->getTable('brand/brand')}` VALUES (1, 'Brand 1', 'NA', 5, 'http://www.site1.com');
    INSERT INTO `{$installer->getTable('brand/brand')}` VALUES (2, 'Brand 2', 'RO', 6, 'http://www.site2.com');
    INSERT INTO `{$installer->getTable('brand/brand')}` VALUES (3, 'Brand 3', 'GB', 7, 'http://www.site3.com');
");
$installer->endSetup();