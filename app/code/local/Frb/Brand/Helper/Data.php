<?php
class Frb_Brand_Helper_Data extends Mage_Core_Helper_Abstract
{

    const PATH = 'brand/brands/brand_level';

    /*
     * This function returns the configured brand level.
     */
    public function getBrandConfig ()
    {
        return Mage::getStoreConfig(self::PATH);
    }
}